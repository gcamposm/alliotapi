# README Staff Internal API

## Description
This project is created to complete a technical test for Alliot

## Software requirements

|                         |                        |
|-------------------------|------------------------|
| Programming language    | Ruby (2.7.6)           |
| Framework               | Rails Rails (13.0.6)   |
| Database                | PostgreSQL (12)        |

## Useful information

|                         |                        |
|-------------------------|------------------------|
| Database                | alliotApi_development  |
| Server PORT             | 3000                   |

## General steps to run the project.
1. Fulfill the Software Requirements:
    - [Install PostgreSQL 12](https://www.postgresql.org/download/macosx/).
    - [Install Ruby 2.7.6](https://rvm.io/rvm/install).
    - Install Rails 13.0.6 (through Bundler at step 3).

2. Install the repository on your local directory [via GIT](https://docs.github.com/en/github/getting-started-with-github/getting-started-with-git/about-remote-repositories).

3. Install the project's gems via Bundler.
    ```Ruby
    $ gem install bundler; bundle install
    ```

    >**Recommendation**: Once installed the project, create a specific gemset

    ```Ruby
    ## Optional
    $ rvm gemset use 2.7.6@alliot --create
    ```

4. Run your development server on port 3000.
    ```Ruby
    $ rails s
    ```
