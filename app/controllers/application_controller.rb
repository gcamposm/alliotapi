class ApplicationController < ActionController::Base
  acts_as_token_authentication_handler_for User, fallback: :none

  def render_rescue(exception)
    Rails.logger.info { "ERROR: #{exception.message}\nBUGTRACE#{exception.backtrace[0]}" }
    render json: { message: exception.message, state: :error }, status: :bad_request
  end
end
