module V
  class ApplicationController < ActionController::Base
    acts_as_token_authentication_handler_for User, fallback: :none
    after_action :cors_set_access_control_headers
    skip_before_action :verify_authenticity_token

    def cors_set_access_control_headers
      headers['Access-Control-Allow-Origin']  = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token, X-Alliot-Email, X-Alliot-Access-Token'
      headers['Access-Control-Max-Age']       = '1728000'
    end

    def render_rescue(exception)
      Rails.logger.info { "ERROR: #{exception.message}\nBUGTRACE#{exception.backtrace[0]}" }
      render json: { message: exception.message, state: :error }, status: :bad_request
    end
  end
end
