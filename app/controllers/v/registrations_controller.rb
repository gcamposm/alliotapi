class V::RegistrationsController < V::ApplicationController
  def create
    User.transaction do
      @user = User.new(user_params)
      raise ActiveRecord::Rollback, @user.errors.to_s unless @user.save

      @user
    end
  rescue StandardError => e
    render_rescue(e)
  end

  private

  def user_params
    params.require(:user).permit(User.allowed_attributes)
  end
end
