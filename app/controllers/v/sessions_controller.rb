class V::SessionsController < V::ApplicationController
  def create
    @user = User.find_by(email: email)
    raise 'user not found' if @user.blank?

    if @user.valid_password?(password)
      render 'v/v1/users/login', status: :ok
    else
      render status: :unauthorized, json: { message: I18n.t('devise.failure.invalid') }
    end
  rescue StandardError => e
    render_rescue(e)
  end

  private

  def user_params
    params.require(:user)
  end

  def email
    user_params[:email]
  end

  def username
    user_params[:username]
  end

  def password
    user_params[:password]
  end
end
