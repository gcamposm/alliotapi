class V::V1::TasksController < V::ApplicationController
  before_action :set_tasks, only: [:index]
  before_action :set_task, only: %i[update archive]

  def index
    @tasks
  rescue StandardError => e
    render_rescue(e)
  end

  def create
    Task.transaction do
      @task = current_user.tasks.new(task_params)
      raise ActiveRecord::RecordNotUnique unless @task.valid?

      @task.save
    end
    raise if @task.blank?

    @task
  rescue ActiveRecord::RecordNotUnique => e
    render_rescue(e)
  end

  def update
    Task.transaction do
      raise 'No se pudo actualizar la tarea' unless @task.update(task_params)

      @task
    end
  rescue StandardError => e
    render_rescue(e)
  end

  def archive
    Task.transaction do
      raise 'No se pudo eliminar la tarea' unless @task.destroy
    end
    render json: { state: :ok, message: 'Tarea eliminada correctamente' }, status: :ok
  rescue StandardError => e
    render_rescue(e)
  end

  private

  def set_task
    @task ||= Task.find(params[:id])
  end

  def set_tasks
    @tasks ||= current_user.tasks
  end

  def task_params
    params.require(:task).permit(Task.allowed_attributes)
  end
end
