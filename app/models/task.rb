class Task < ApplicationRecord
  # RELATIONS
  belongs_to :user
  acts_as_paranoid

  def self.allowed_attributes
    %i[title description completed]
  end
end
