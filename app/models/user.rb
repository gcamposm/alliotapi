class User < ApplicationRecord
  # ENABLE API AUTHENTICATION
  acts_as_token_authenticatable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # RELATIONS
  has_many :tasks

  def self.allowed_attributes
    %i[username email password]
  end
end
