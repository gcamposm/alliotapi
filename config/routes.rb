require 'api_constraints'

Rails.application.routes.draw do
  scope '/', defaults: { format: :json } do
    devise_for :user, controllers: { sessions: 'v/sessions', registrations: 'v/registrations', passwords: 'v/passwords' }
  end
  namespace :v, defaults: { format: :json } do
    post 'sign_up', to: 'registrations#create', as: 'sign_up'
    post 'login', to: 'sessions#create', as: 'login'
    scope module: :v1, constraints: APIConstraints.new(version: 1) do
      resources :tasks do
        member do
          post 'archive', action: :archive
        end
      end
    end
  end
end
