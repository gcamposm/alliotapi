class APIConstraints
  attr_accessor :version

  def initialize(options)
    @default = options[:default]
    @version = options[:version]
  end

  def matches?(req)
    @default || req.headers['Accept'].include?("application/vnd.alliot.v#{@version}")
  rescue StandardError => _e
    @version = 1
    @default = false
  end
end
